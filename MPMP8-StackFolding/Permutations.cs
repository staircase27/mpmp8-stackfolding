﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MPMP8_StackFolding
{
    /// <summary>
    /// Helper class to generate permutations of a collection.
    /// </summary>
    public static class Permutations
    {
        /// <summary>
        /// Swap two elements.
        /// </summary>
        /// <typeparam name="T"> The type of the elements to swap. </typeparam>
        /// <param name="a"> The first element to swap. </param>
        /// <param name="b"> The second element to swap. </param>
        public static void Swap<T>(ref T a, ref T b)
        {
            var tmp = a;
            a = b;
            b = tmp;
        }


        /// <summary>
        /// Generate all permutations of the input collection.
        /// </summary>
        /// <typeparam name="T"> The type of element in the collection. </typeparam>
        /// <param name="elements"> The elements to permute </param>
        /// <returns> Each permutation of the input collection. </returns>
        public static IEnumerable<IReadOnlyList<T>> GetAllPermutations<T>(this IEnumerable<T> elements)
        {
            var list = elements.ToArray();
            return list.GetAllPermutations(0, list.Length);
        }


        /// <summary>
        /// Generate all permutations of the <paramref name="count"/> elements starting
        /// with <paramref name="start>"/> element of the input collection.
        /// </summary>
        /// <typeparam name="T"> The type of element in the collection. </typeparam>
        /// <param name="elements"> The elements to permute </param>
        /// <param name="start"> The index of the first element to permute. </param>
        /// <param name="count"> How many elements to permute </param>
        /// <returns> Each permutation of the input collection. </returns>
        public static IEnumerable<IReadOnlyList<T>> GetAllPermutations<T>(this IEnumerable<T> elements, int start,
            int count)
        {
            var list = elements.ToArray();
            if (start < 0)
                throw new ArgumentOutOfRangeException(nameof(start), start, "Start can't be less than zero.");
            if (start >= list.Length)
                throw new ArgumentOutOfRangeException(nameof(start), start,
                    "Start can't be greater than the number of elements.");
            if (start + count > list.Length)
                throw new ArgumentOutOfRangeException(nameof(count), count,
                    "Not enough elements in the collection for the specified count.");
            return list.GetAllPermutations(start, start + count);
        }


        /// <summary>
        /// Generate all permutations of the elements from <paramref name="start"/> to
        /// the end of the input collection.
        /// </summary>
        /// <typeparam name="T"> The type of element in the collection. </typeparam>
        /// <param name="elements"> The elements to permute </param>
        /// <param name="start"> The index of the first element to permute. </param>
        /// <returns> Each permutation of the input collection. </returns>
        public static IEnumerable<IReadOnlyList<T>> GetAllPermutations<T>(this IEnumerable<T> elements, int start)
        {
            var list = elements.ToArray();
            if (start < 0)
                throw new ArgumentOutOfRangeException(nameof(start), start, "Start can't be less than zero.");
            if (start >= list.Length)
                throw new ArgumentOutOfRangeException(nameof(start), start,
                    "Start can't be greater than the number of elements.");
            return list.GetAllPermutations(start, list.Length);
        }


        /// <summary>
        /// Generate all permutations of the elements in range [<paramref name="i"/>,
        /// <paramref name="j"/>) of the collection <paramref name="elements"/>.
        /// </summary>
        /// <typeparam name="T"> The type of element in the collection. </typeparam>
        /// <param name="elements"> The elements to permute </param>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <returns></returns>
        private static IEnumerable<IReadOnlyList<T>> GetAllPermutations<T>(this T[] elements, int i, int j)
        {
            if (i == j)
            {
                // If all elements have been permuted then return a copy of the input.
                yield return elements.ToArray();
                yield break;
            }

            for (var k = i; k < j; k++)
            {
                // elements[k] is being used for position i in the result so swap and
                // then recurse. (Note first swap is a no-op)
                Swap(ref elements[i], ref elements[k]);
                foreach (var permutation in elements.GetAllPermutations(i + 1, j))
                    yield return permutation;
                // Don't need to swap back as will just end up shifting all elements in
                // range [i,k-1) to [i+1,k). (e.g. if i = 0 and j = 6 the
                // elements will progress through the states 0,1,2,3,4,5 -> 1,0,2,3,4,5
                // -> 2,0,1,3,4,5 -> 3,0,1,2,4,5 -> ...
            }
        }
    }
}