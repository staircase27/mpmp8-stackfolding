﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MPMP8_StackFolding
{
    /// <summary>
    /// An object that represents a possible folded state. The state is defined by the
    /// order each section of the unfolded sheet appears in within the folded stack.
    /// </summary>
    public sealed class FoldedStack : IFormattable, IEquatable<FoldedStack>
    {
        /// <summary>
        /// Create a new Folded Stack instance.
        /// </summary>
        /// <param name="width"> the width of the original sheet. </param>
        /// <param name="height"> the height of the original sheet. </param>
        /// <param name="elements"> the elements in the stack. </param>
        /// <exception cref="ArgumentException"> thrown if <paramref name="width"/> is
        /// less than 1. </exception>
        /// <exception cref="ArgumentException"> thrown if <paramref name="height"/> is
        /// less than 1. </exception>
        /// <exception cref="ArgumentException"> thrown if <paramref name="elements"/>
        /// doesn't contain every possible valid element (<see cref="Element.X"/> in [0,
        /// <paramref name="width"/>) and <see cref="Element.Y"/> in [0,
        /// <paramref name="height"/>)) exactly once. </exception>
        public FoldedStack(int width, int height, IReadOnlyList<Element> elements)
        {
            if (width < 1) throw new ArgumentException("Width must be greater than 1", nameof(width));
            if (height < 1) throw new ArgumentException("Width must be greater than 1", nameof(height));
            if (elements.Count != width * height)
                throw new ArgumentException("All elements must be used.", nameof(elements));
            _width = width;
            _height = height;
            _elements = elements;
            var dictionary = new Dictionary<Element, int>();
            for (var index = 0; index < elements.Count; index++)
            {
                if (elements[index].X < 0)
                    throw new ArgumentException("Element X coordinate must be greater than 0",
                        $"{nameof(elements)}[{index}].{nameof(Element.X)}");
                if (elements[index].Y < 0)
                    throw new ArgumentException("Element Y coordinate must be greater than 0",
                        $"{nameof(elements)}[{index}].{nameof(Element.Y)}");
                if (elements[index].X >= width)
                    throw new ArgumentException("Element X coordinate must be less than the width",
                        $"{nameof(elements)}[{index}].{nameof(Element.X)}");
                if (elements[index].Y >= height)
                    throw new ArgumentException("Element Y coordinate must be less than the height",
                        $"{nameof(elements)}[{index}].{nameof(Element.Y)}");
                dictionary[elements[index]] = index;
            }

            if (dictionary.Count != elements.Count) throw new ArgumentException("Duplicate element", nameof(elements));
            _lookup = dictionary;
        }


        /// <summary>
        /// Generate all stacks of the specified size without checking if they can be
        /// produced by folding.
        /// </summary>
        /// <param name="width"> the width of the original sheet. </param>
        /// <param name="height"> the height of the original sheet. </param>
        /// <returns> A sequence containing all stacks. </returns>
        public static IEnumerable<FoldedStack> GenerateAll(int width, int height) =>
            Enumerable.Range(0, width)
                .SelectMany(i => Enumerable.Range(0, height).Select(j => new Element(i, j)))
                .GetAllPermutations()
                .Select(elements => new FoldedStack(width, height, elements));


        /// <summary>
        /// Generate all stacks of the specified size with the element 0,0 fixed as the
        /// front of the stack without checking if they can be produced by folding.
        /// </summary>
        /// <param name="width"> the width of the original sheet. </param>
        /// <param name="height"> the height of the original sheet. </param>
        /// <returns> A sequence containing all stacks. </returns>
        public static IEnumerable<FoldedStack> GenerateAllWithZeroFixed(int width, int height) =>
            Enumerable.Range(0, width)
                .SelectMany(i => Enumerable.Range(0, height).Select(j => new Element(i, j)))
                .GetAllPermutations(1)
                .Select(elements => new FoldedStack(width, height, elements));


        /// <summary>
        /// Generate all stacks of the specified size that can be produced by folding.
        ///
        /// This works by producing all possible stacks and then checking if each is
        /// possible which is inefficient. Use
        /// <see cref="StackBuilder.GenerateValidStacks"/> instead.
        /// </summary>
        /// <param name="width"> the width of the original sheet. </param>
        /// <param name="height"> the height of the original sheet. </param>
        /// <returns> A sequence containing all stacks. </returns>
        public static IEnumerable<FoldedStack> GenerateAllValid(int width, int height) =>
            GenerateAll(width, height).AsParallel().Where(stack => stack.CheckLinks());


        /// <summary>
        /// Generate all stacks of the specified size with the element 0,0 fixed as the
        /// front of the stack that can be produced by folding.
        ///
        /// This works by producing all possible stacks and then checking if each is
        /// possible which is inefficient. Use
        /// <see cref="StackBuilder.GenerateValidStacks"/> instead.
        /// </summary>
        /// <param name="width"> the width of the original sheet. </param>
        /// <param name="height"> the height of the original sheet. </param>
        /// <returns> A sequence containing all stacks. </returns>
        public static IEnumerable<FoldedStack> GenerateAllValidWithZeroFixed(int width, int height) =>
            GenerateAllWithZeroFixed(width, height).AsParallel().Where(stack => stack.CheckLinks());


        /// <summary>
        /// Convert the stack to a string representation using 
        /// <see cref="FormatStack()"/>.
        /// </summary>
        /// <returns> A string representation of the stack. </returns>
        public override string ToString() => ToString(null, null);


        /// <summary>
        /// Convert the stack to a string representation using either
        /// <see cref="FormatStack()"/> or
        /// <see cref="FormatSheet()"/> depending on
        /// <paramref name="format"/>.
        /// </summary>
        /// <param name="format"> select the format to use. </param>
        /// <returns> A string representation of the stack. </returns>
        public string ToString(string format) => ToString(format, null);


        /// <summary>
        /// Convert the stack to a string representation using
        /// <see cref="FormatStack(IFormatProvider)"/>.
        /// </summary>
        /// <param name="formatProvider"> The format provider used to format the values. </param>
        /// <returns> A string representation of the stack. </returns>
        public string ToString(IFormatProvider formatProvider) => ToString(null, formatProvider);


        /// <summary>
        /// Convert the stack to a string representation using either
        /// <see cref="FormatStack(IFormatProvider)"/> or
        /// <see cref="FormatSheet(IFormatProvider)"/> depending on
        /// <paramref name="format"/>.
        /// </summary>
        /// <param name="format"> select the format to use. </param>
        /// <param name="formatProvider"> The format provider used to format the values. </param>
        /// <returns> A string representation of the stack. </returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            switch (format)
            {
                case null:
                case "":
                case "t":
                case "T":
                    return FormatStack(formatProvider);
                case "h":
                case "H":
                    return FormatSheet(formatProvider);
            }

            throw new ArgumentException("Invalid format code");
        }


        /// <summary>
        /// Format this stack as list of the elements in stack order. This is the format
        /// used as input when constructing this instance.
        ///
        /// This format can be selected using the format code 't' and is the default if
        /// no format code is used.
        /// </summary>
        /// <returns> A single line description of the stack </returns>
        public string FormatStack() => FormatStack(null);


        /// <summary>
        /// Format this stack as list of the elements in stack order. This is the format
        /// used as input when constructing this instance.
        ///
        /// This format can be selected using the format code 't' and is the default if
        /// no format code is used.
        /// </summary>
        /// <param name="formatProvider"> The format provider used to format the values. </param>
        /// <returns> A single line description of the stack </returns>
        public string FormatStack(IFormatProvider formatProvider) => string.Join(", ", _elements.Select(element =>
            string.Format(formatProvider, "({0})", element)));


        /// <summary>
        /// Format this stack as a sheet with each space on the grid labelled by it's
        /// index in the stack. This is the format used in the original problem but with
        /// numbers rather than letters.
        ///
        /// This format can be selected using the format code 'h'.
        /// </summary>
        /// <returns> A multi line string that labels the stack. </returns>
        public string FormatSheet() => FormatStack(null);


        /// <summary>
        /// Format this stack as a sheet with each space on the grid labelled by it's
        /// index in the stack. This is the format used in the original problem but with
        /// numbers rather than letters.
        ///
        /// This format can be selected using the format code 'h'.
        /// </summary>
        /// <param name="formatProvider"> The format provider used to format the values. </param>
        /// <returns> A multi line string that labels the stack. </returns>
        public string FormatSheet(IFormatProvider formatProvider)
        {
            var builder = new StringBuilder();
            builder.AppendFormat(formatProvider, "{0}", _lookup[new Element(0, 0)]);
            for (var i = 1; i < _width; i++)
                builder.AppendFormat(formatProvider, ",{0}", _lookup[new Element(i, 0)]);

            for (var j = 1; j < _height; j++)
            {
                builder.AppendFormat(formatProvider, "\n{0}", _lookup[new Element(0, j)]);
                for (var i = 1; i < _width; i++)
                    builder.AppendFormat(formatProvider, ",{0}", _lookup[new Element(i, j)]);
            }

            return builder.ToString();
        }


        /// <summary>
        /// Check if the specified stack can be generated by folding by checking if the
        /// links along each edge of the stack intersect.
        /// </summary>
        /// <returns> is the stack possible to create by folding. </returns>
        public bool CheckLinks()
        {
            var topLeftLinks = new SortedDictionary<int, int>();
            var bottomRightLinks = new SortedDictionary<int, int>();

            // First we check the left and right edges of the stack.
            for (var i = 0; i < _width - 1; ++i)
            for (var j = 0; j < _height; ++j)
                // Find all the horizontal links by scanning through the grid taking each
                // pair of horizontally adjacent elements and marking that there is a
                // link between the relevant layers in the stack on the appropriate side.
                // The relevant side is selected by observing that links will be on
                // alternate sides for each segment as you work across the sheet.
                AddLowToHigh(i % 2 == 0 ? bottomRightLinks : topLeftLinks, _lookup[new Element(i, j)],
                    _lookup[new Element(i + 1, j)]);

            if (!CheckLinks(topLeftLinks)) return false;
            if (!CheckLinks(bottomRightLinks)) return false;

            topLeftLinks.Clear();
            bottomRightLinks.Clear();

            for (var i = 0; i < _width; ++i)
            for (var j = 0; j < _height - 1; ++j)
                // Find all the vertical links by scanning through the grid taking each
                // pair of vertical adjacent elements and marking that there is a link
                // between the relevant layers in the stack on the appropriate side.
                // The relevant side is selected by observing that links will be on
                // alternate sides for each segment as you work down the sheet.
                AddLowToHigh(j % 2 == 0 ? bottomRightLinks : topLeftLinks, _lookup[new Element(i, j)],
                    _lookup[new Element(i, j + 1)]);

            if (!CheckLinks(topLeftLinks)) return false;
            if (!CheckLinks(bottomRightLinks)) return false;

            return true;
        }


        public bool Equals(FoldedStack other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _width == other._width && _height == other._height && _elements.SequenceEqual(other._elements);
        }


        public override bool Equals(object obj) =>
            ReferenceEquals(this, obj) || obj is FoldedStack other && Equals(other);


        public override int GetHashCode()
        {
            var code = new HashCode();
            code.Add(_width);
            code.Add(_height);
            foreach (var element in _elements) code.Add(element);
            return code.ToHashCode();
        }


        public static bool operator ==(FoldedStack left, FoldedStack right) => Equals(left, right);

        public static bool operator !=(FoldedStack left, FoldedStack right) => !Equals(left, right);


        /// <summary>
        /// Check if the links intersect.
        /// </summary>
        /// <param name="links"> The links to check. </param>
        /// <returns> <see langref="false"/> if the links intersect and
        /// <see langref="true"/> if they don't. </returns>
        private static bool CheckLinks(SortedDictionary<int, int> links)
        {
            var stack = new Stack<int>();
            foreach (var (key, value) in links)
            {
                // And previous link that ends before this new link is no longer relevant.
                while (stack.Count > 0 && key > stack.Peek())
                    stack.Pop();
                // If there any previous links that end between the start and end of this
                // link then we have an intersection.
                if (stack.Count > 0 && value > stack.Peek())
                    return false;
                // Add the end of this link to the stack so any links that start before
                // the end of this link can be checked against this link.
                stack.Push(value);
            }

            // No intersections so all good.
            return true;
        }


        /// <summary>
        /// Add the mapping between <paramref name="i"/> and <paramref name="j"/> such
        /// that the lower one is the key and the higher one is the value.
        /// </summary>
        /// <param name="links"> The links collection to add the mapping to. </param>
        /// <param name="i"> The first of the two values for the mapping. </param>
        /// <param name="j"> The second of the two values for the mapping. </param>
        private static void AddLowToHigh(IDictionary<int, int> links, int i, int j)
        {
            if (i < j)
                links.Add(i, j);
            else
                links.Add(j, i);
        }


        private readonly int _width;
        private readonly int _height;
        private readonly IReadOnlyCollection<Element> _elements;
        private readonly IReadOnlyDictionary<Element, int> _lookup;
    }
}