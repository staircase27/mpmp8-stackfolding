﻿using System;

namespace MPMP8_StackFolding
{
    /// <summary>
    /// A integer valued point. Used to label the spaces on the unfolded sheet of paper.
    /// </summary>
    public readonly struct Element : IEquatable<Element>, IFormattable
    {
        public Element(int x, int y)
        {
            X = x;
            Y = y;
        }


        public bool Equals(Element other) => X == other.X && Y == other.Y;

        public override bool Equals(object obj) => obj is Element other && Equals(other);

        public override int GetHashCode() => HashCode.Combine(X, Y);

        public override string ToString() => ToString(null);

        public string ToString(IFormatProvider formatProvider) => string.Format(formatProvider, "{0},{1}", X, Y);

        public static bool operator ==(Element left, Element right) => left.Equals(right);

        public static bool operator !=(Element left, Element right) => !left.Equals(right);

        /// <summary>
        /// The horizontal coordinate of the element.
        /// </summary>
        public int X { get; }

        /// <summary>
        /// The vertical coordinate of the element.
        /// </summary>
        public int Y { get; }

        string IFormattable.ToString(string format, IFormatProvider formatProvider) => ToString(formatProvider);
    }
}