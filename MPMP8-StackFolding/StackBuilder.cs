﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace MPMP8_StackFolding
{
    /// <summary>
    /// This class implements a more advanced and more efficient algorithm to generate
    /// valid folded stacks.
    /// </summary>
    public static class StackBuilder
    {
        /// <summary>
        /// Generate all stacks of the specified size that can be produced by folding.
        /// </summary>
        /// <param name="width"> the width of the original sheet. </param>
        /// <param name="height"> the height of the original sheet. </param>
        /// <param name="zeroFront"> is the 0,0 element forced to be at the front of the
        /// stack. </param>
        /// <returns> A sequence containing all stacks. </returns>
        public static IEnumerable<FoldedStack> GenerateValidStacks(int width, int height, bool zeroFront)
        {
            // This generates all stacks by using a doubly linked list of the elements,
            // with up for 4 extra links for the sheets the element is connected to by
            // folds. Each element is inserted into the list one at a time trying each
            // possible valid space one after the other.
            //
            // As with the original basic algorithm a sheet is allowed in a position if
            // and only it if wouldn't require any folds to intersect other folds. This
            // check is relatively easy for elements in the top row or left column but is
            // a bit more complicated when an element has existing neighbours both above
            // and to its left.

            // Marker head node for the linked list of elements in the stack.
            var head = new Node(new Element(int.MinValue, int.MinValue));
            // The first node.
            var startNode = new Node(new Element(0, 0));
            // A lookup used to find the neighbour elements when adding a new element.
            var lookup = new Dictionary<Element, Node> {[startNode.Element] = startNode};
            head.Next = head.Previous = startNode;
            startNode.Next = startNode.Previous = head;
            startNode.Index = 0;
            // Handle the second element specially as it's options aren't constrained by links.
            var nextNode = new Node(new Element(1, 0));
            lookup[nextNode.Element] = nextNode;
            nextNode.Links[Right] = startNode;
            startNode.Links[Right] = nextNode;
            if (zeroFront)
            {
                // If 0,0 is fixed as the front then we only have a single place the
                // second element can go.
                AddAfter(nextNode, startNode, head);
                foreach (var result in GenerateValidStacksRecurse(width, height, head, startNode, lookup, 2))
                    yield return result;
            }
            else
            {
                // If 0,0 is not fixed as the front then the second element can either
                // before or after 0,0.
                AddAfter(nextNode, head, head);
                foreach (var result in GenerateValidStacksRecurse(width, height, head, head, lookup, 2))
                    yield return result;
                MoveToAfterLater(nextNode, startNode);
                foreach (var result in GenerateValidStacksRecurse(width, height, head, head, lookup, 2))
                    yield return result;
            }
        }


        /// <summary>
        /// The main recursion point in the algorithm. This is called recursively for
        /// each element in the grid and selects the correct method to handle calculating
        /// the valid points for the element to be added.
        /// </summary>
        /// <param name="width"> the width of the original sheet. </param>
        /// <param name="height"> the height of the original sheet. </param>
        /// <param name="head"> the head element for the linked list. </param>
        /// <param name="frontLimit"> the element that no other elements can go in front
        /// of. This will be either <paramref name="head"/> or <paramref name="head"/>.
        /// <see cref="Node.Next"/> depending on if 0,0 is fixed as the front of the
        /// stack.</param>
        /// <param name="lookup"> lookup used to find the neighbour elements when adding
        /// a new element. </param>
        /// <param name="index"> The index of the element that is being added. The
        /// coordinates of the element are index % width, index / width. </param>
        /// <returns> a sequence of all the valid stacks with the relative order of any
        /// already added elements unchanged. </returns>
        private static IEnumerable<FoldedStack> GenerateValidStacksRecurse(in int width, in int height, Node head,
            in Node frontLimit, Dictionary<Element, Node> lookup, int index)
        {
            // Calculate the coordinates of the point.
            var j = Math.DivRem(index, width, out var i);
            if (j == height)
                // We have finished recursing so output this state.
                return new[] {ToFoldedStack(width, height, head)};
            // Either reuse or create new node.
            if (!lookup.TryGetValue(new Element(i, j), out var newNode))
                lookup[new Element(i, j)] = newNode = new Node(new Element(i, j));

            if (j == 0)
                // This is the top row.
                return GenerateValidStacksRecurseTopRow(width, height, head, frontLimit, lookup, index, newNode,
                    lookup[new Element(i - 1, j)], i % 2);

            if (i == 0)
                // This is the left column.
                return GenerateValidStacksRecurseLeftColumn(width, height, head, frontLimit, lookup, index, newNode,
                    lookup[new Element(i, j - 1)], j % 2 | 2);

            // This is a general elements with neighbours in both directions.
            return GenerateValidStacksRecurseGeneral(width, height, head, frontLimit, lookup, index, newNode,
                lookup[new Element(i - 1, j)], lookup[new Element(i, j - 1)], i % 2, (j % 2) | 2);
        }


        /// <summary>
        /// Try add a new element that is on the top row to the stack.
        /// </summary>
        /// <param name="width"> the width of the original sheet. </param>
        /// <param name="height"> the height of the original sheet. </param>
        /// <param name="head"> the head element for the linked list. </param>
        /// <param name="frontLimit"> the element that no other elements can go in front
        /// of. This will be either <paramref name="head"/> or <paramref name="head"/>.
        /// <see cref="Node.Next"/> depending on if 0,0 is fixed as the front of the
        /// stack.</param>
        /// <param name="lookup"> lookup used to find the neighbour elements when adding
        /// a new element. </param>
        /// <param name="index"> The index of the element that is being added. The
        /// coordinates of the element are index % width, index / width. </param>
        /// <param name="newNode"> The new node to add. </param>
        /// <param name="linkedTo"> The node that the new node is linked to. </param>
        /// <param name="linkDirection"> The side of the stack that the two nodes are
        /// linked on. </param>
        /// <returns> a sequence of all the valid stacks with the relative order of any
        /// already added elements unchanged. </returns>
        private static IEnumerable<FoldedStack> GenerateValidStacksRecurseTopRow(int width, int height, Node head,
            Node frontLimit, Dictionary<Element, Node> lookup, int index, Node newNode, Node linkedTo,
            int linkDirection)
        {
            // Add the links for the node.
            newNode.Links[linkDirection] = linkedTo;
            linkedTo.Links[linkDirection] = newNode;
            // Add the link to the stack just before the node it is linked to.
            AddBefore(newNode, linkedTo, head);
            while (true)
            {
                foreach (var result in GenerateValidStacksRecurse(width, height, head, frontLimit, lookup, index + 1))
                    yield return result;
                // See if there are any other possible locations walking towards the
                // start of the stack.
                if (!TryGetNextCandidateBefore(newNode, linkDirection, frontLimit, newNode, out var nextBefore)) break;
                MoveToBeforeEarlier(newNode, nextBefore);
            }

            // Move the link to just after the linked node in the stack.
            MoveToAfterLater(newNode, linkedTo);
            while (true)
            {
                foreach (var result in GenerateValidStacksRecurse(width, height, head, frontLimit, lookup, index + 1))
                    yield return result;
                // See if they are any other possible locations walking towards the end
                // of the stack.
                if (!TryGetNextCandidateAfter(newNode, linkDirection, head, newNode, out var nextAfter)) break;
                MoveToAfterLater(newNode, nextAfter);
            }

            // Tidy up.
            RemoveNode(newNode, head);
            linkedTo.Links[linkDirection] = null;
        }


        /// <summary>
        /// Try add a new element that is in the first column to the stack.
        /// </summary>
        /// <param name="width"> the width of the original sheet. </param>
        /// <param name="height"> the height of the original sheet. </param>
        /// <param name="head"> the head element for the linked list. </param>
        /// <param name="frontLimit"> the element that no other elements can go in front
        /// of. This will be either <paramref name="head"/> or <paramref name="head"/>.
        /// <see cref="Node.Next"/> depending on if 0,0 is fixed as the front of the
        /// stack.</param>
        /// <param name="lookup"> lookup used to find the neighbour elements when adding
        /// a new element. </param>
        /// <param name="index"> The index of the element that is being added. The
        /// coordinates of the element are index % width, index / width. </param>
        /// <param name="newNode"> The new node to add. </param>
        /// <param name="linkedTo"> The node that the new node is linked to. </param>
        /// <param name="linkDirection"> The side of the stack that the two nodes are
        /// linked on. </param>
        /// <returns> a sequence of all the valid stacks with the relative order of any
        /// already added elements unchanged. </returns>
        private static IEnumerable<FoldedStack> GenerateValidStacksRecurseLeftColumn(int width, int height, Node head,
            Node frontLimit, Dictionary<Element, Node> lookup, int index, Node newNode, Node linkedTo,
            int linkDirection)
        {
            // Add the links for the node.
            newNode.Links[linkDirection] = linkedTo;
            linkedTo.Links[linkDirection] = newNode;
            // Check if in front of the linked node is allowed (Only possible for element
            // at 0,1).
            if (linkedTo == frontLimit)
                // Add the link to the stack just after the node it is linked to.
                AddAfter(newNode, linkedTo, head);
            else
            {
                // Add the link to the stack just before the node it is linked to.
                AddBefore(newNode, linkedTo, head);
                while (true)
                {
                    foreach (var result in GenerateValidStacksRecurse(width, height, head, frontLimit, lookup,
                        index + 1))
                        yield return result;
                    // See if there are any other possible locations walking towards the
                    // start of the stack.
                    if (!TryGetNextCandidateBefore(newNode, linkDirection, frontLimit, newNode, out var nextBefore))
                        break;
                    MoveToBeforeEarlier(newNode, nextBefore);
                }

                // Move the link to just after the linked node in the stack.
                MoveToAfterLater(newNode, linkedTo);
            }

            while (true)
            {
                foreach (var result in GenerateValidStacksRecurse(width, height, head, frontLimit, lookup, index + 1))
                    yield return result;
                // See if they are any other possible locations walking towards the end
                // of the stack.
                if (!TryGetNextCandidateAfter(newNode, linkDirection, head, newNode, out var nextAfter)) break;
                MoveToAfterLater(newNode, nextAfter);
            }

            // Tidy up.
            RemoveNode(newNode, head);
            linkedTo.Links[linkDirection] = null;
        }


        /// <summary>
        /// Try add a new element that has links to existing elements both horizontally
        /// and vertically.
        /// </summary>
        /// <param name="width"> the width of the original sheet. </param>
        /// <param name="height"> the height of the original sheet. </param>
        /// <param name="head"> the head element for the linked list. </param>
        /// <param name="frontLimit"> the element that no other elements can go in front
        /// of. This will be either <paramref name="head"/> or <paramref name="head"/>.
        /// <see cref="Node.Next"/> depending on if 0,0 is fixed as the front of the
        /// stack.</param>
        /// <param name="lookup"> lookup used to find the neighbour elements when adding
        /// a new element. </param>
        /// <param name="index"> The index of the element that is being added. The
        /// coordinates of the element are index % width, index / width. </param>
        /// <param name="newNode"> The new node to add. </param>
        /// <param name="linkedToH"> The node that the new node is linked to horizontally. </param>
        /// <param name="linkedToV"> The node that the new node is linked to vertically. </param>
        /// <param name="linkDirectionH"> The side of the stack that the two horizontally
        /// linked nodes are linked on. </param>
        /// <param name="linkDirectionV"> The side of the stack that the two vertically
        /// linked nodes are linked on. </param>
        /// <returns> a sequence of all the valid stacks with the relative order of any
        /// already added elements unchanged. </returns>
        private static IEnumerable<FoldedStack> GenerateValidStacksRecurseGeneral(int width, int height, Node head,
            Node frontLimit, Dictionary<Element, Node> lookup, int index, Node newNode, Node linkedToH, Node linkedToV,
            int linkDirectionH, int linkDirectionV)
        {
            // Add the links for the node.
            newNode.Links[linkDirectionH] = linkedToH;
            linkedToH.Links[linkDirectionH] = newNode;
            newNode.Links[linkDirectionV] = linkedToV;
            linkedToV.Links[linkDirectionV] = newNode;
            // Calculate the earliest possible place the node could go in the stack for
            // the horizontal and vertical links independently.
            var firstAfterV = GetFirstCandidateAfter(linkedToV, frontLimit, linkDirectionV);
            var firstAfterH = GetFirstCandidateAfter(linkedToH, frontLimit, linkDirectionH);
            // Try get a position that is valid with both links.
            if (TryGetMutualCandidate(firstAfterV, firstAfterH, linkDirectionV, linkDirectionH, head, newNode,
                out var after))
            {
                // Add the new node at the first valid location.
                AddAfter(newNode, after, head);
                while (true)
                {
                    foreach (var result in GenerateValidStacksRecurse(width, height, head, frontLimit, lookup,
                        index + 1))
                        yield return result;
                    // See if they are any other possible locations walking towards the
                    // end of the stack.
                    if (!TryGetNextMutualCandidate(newNode, linkDirectionV, linkDirectionH, head,
                        newNode, out after)) break;
                    MoveToAfterLater(newNode, after);
                }

                // Remove the node to tidy up.
                RemoveNode(newNode, head);
            }

            // Tidy up.
            linkedToH.Links[linkDirectionH] = null;
            linkedToV.Links[linkDirectionV] = null;
        }


        /// <summary>
        /// Try get a node that is valid with both directions of link.
        /// </summary>
        /// <param name="vAfter"> The first node to consider when checking for valid
        ///     nodes in the vertical direction. </param>
        /// <param name="hAfter"> The first node to consider when checking for valid
        ///     nodes in the horizontal direction. </param>
        /// <param name="linkDirectionH"> The side of the stack that the two horizontally
        /// linked nodes are linked on. </param>
        /// <param name="linkDirectionV"> The side of the stack that the two vertically
        /// linked nodes are linked on. </param>
        /// <param name="head"> The head of the linked list. Used to stop iteration. </param>
        /// <param name="newNode"> The new node that has been added. Used as links to
        /// this node should be ignored. </param>
        /// <param name="after"> The node that is the new candidate insertion point. </param>
        /// <returns> was a valid insertion point found. </returns>
        private static bool TryGetMutualCandidate(Node vAfter, Node hAfter, int linkDirectionV,
            int linkDirectionH, Node head, Node newNode, out Node after)
        {
            while (true)
            {
                after = vAfter;
                if (vAfter.Index < hAfter.Index)
                {
                    // hAfter is earlier so try and get a later option.
                    if (!TryGetNextCandidateAfter(vAfter, linkDirectionV, head, newNode, out vAfter))
                        return false;
                }
                else if (vAfter.Index > hAfter.Index)
                {
                    // vAfter is earlier so try and get a later option.
                    if (!TryGetNextCandidateAfter(hAfter, linkDirectionH, head, newNode, out hAfter))
                        return false;
                }
                else
                    // Both vAfter and hAfter are the same so we are done.
                    return true;
            }
        }


        /// <summary>
        /// Try get a node the next node that is valid with both directions of link.
        /// </summary>
        /// <param name="node"> the current mutually valid location that the search
        /// should start from. </param>
        /// <param name="linkDirectionV"> The side of the stack that the two vertically
        ///     linked nodes are linked on. </param>
        /// <param name="linkDirectionH"> The side of the stack that the two horizontally
        ///     linked nodes are linked on. </param>
        /// <param name="head"> The head of the linked list. Used to stop iteration. </param>
        /// <param name="newNode"> The new node that has been added. Used as links to
        ///     this node should be ignored. </param>
        /// <param name="after"> The node that is the new candidate insertion point. </param>
        /// <returns> was a valid insertion point found. </returns>
        private static bool TryGetNextMutualCandidate(Node node, int linkDirectionV,
            int linkDirectionH, Node head, Node newNode, out Node after) =>
            TryGetNextCandidateAfter(node, linkDirectionV, head, newNode, out after) &&
            TryGetNextCandidateAfter(node, linkDirectionH, head, newNode, out var hAfter) &&
            TryGetMutualCandidate(after, hAfter, linkDirectionV, linkDirectionH, head, newNode, out after);


        /// <summary>
        /// Get the location closest to the start of the stack that the node can be added.
        /// </summary>
        /// <param name="node"> The node that the new node is linked to. </param>
        /// <param name="limit"> the element that no other elements can go in front
        /// of. This will be either the head of the list or the front element of the
        /// list.
        /// </param>
        /// <param name="linkDirection"> The direction that the link being considered is in. </param>
        /// <returns> The candidate insertion point for the new node. </returns>
        private static Node GetFirstCandidateAfter(Node node, Node limit, int linkDirection)
        {
            var firstCandidateAfter = node.Previous;
            while (true)
            {
                // Have we reached the limit by walking?
                if (firstCandidateAfter == limit) break;
                // If there isn't a link on the specified element then we are good to
                // carry on walking.
                if (firstCandidateAfter.Links[linkDirection] == null)
                {
                    firstCandidateAfter = firstCandidateAfter.Previous;
                    continue;
                }

                // If there is a link and it goes to later than the current element then
                // this fold must be outside of the fold we are adding and therefore
                // there are no further options.
                if (firstCandidateAfter.Links[linkDirection].Index > firstCandidateAfter.Index) break;
                // We were considering placing inside a fold so go to the other side of the
                // folded layers.
                var beforeLink = firstCandidateAfter.Links[linkDirection];
                // Check if this new node is the limit and as such we should stop.
                if (beforeLink == limit) break;
                firstCandidateAfter = beforeLink.Previous;
            }

            return firstCandidateAfter;
        }


        /// <summary>
        /// Try find the next node searching towards the front of the stack that is valid
        /// place for the new node.
        /// </summary>
        /// <param name="node"> The current valid location to start checking from. </param>
        /// <param name="linkDirection"> The direction that the link being considered is in. </param>
        /// <param name="limit"> the element that no other elements can go in front
        ///     of. This will be either the head of the list or the front element of the
        ///     list.
        /// </param>
        /// <param name="newNode"> The new node that has been added. Used as links to
        ///     this node should be ignored. </param>
        /// <param name="nextBefore"> The node that is the new candidate insertion point. </param>
        /// <returns> was a valid insertion point found. </returns>
        private static bool TryGetNextCandidateBefore(Node node, int linkDirection, Node limit,
            Node newNode, out Node nextBefore)
        {
            nextBefore = node.Previous;
            // Have we reached the limit by walking?
            if (nextBefore == limit) return false;
            // If there isn't a link on the specified element then we are good to go.
            if (nextBefore.Links[linkDirection] == null || nextBefore.Links[linkDirection] == newNode) return true;
            // If there is a link and it goes to later than the current element then this
            // fold must be outside of the fold we are adding and therefore there are
            // no further options.
            if (nextBefore.Links[linkDirection].Index > nextBefore.Index) return false;
            // We were considering placing inside a fold so go to the other side of the
            // folded layers.
            nextBefore = nextBefore.Links[linkDirection];
            // Check if this new node is the limit and as such we should stop.
            return nextBefore != limit;
        }


        /// <summary>
        /// Try find the next node searching towards the back of the stack that is valid
        /// place for the new node.
        /// </summary>
        /// <param name="node"> The current valid location to start checking from. </param>
        /// <param name="linkDirection"> The direction that the link being considered is in. </param>
        /// <param name="head"> The head of the linked list. Used to stop iteration. </param>
        /// <param name="newNode"> The new node that has been added. Used as links to
        ///     this node should be ignored. </param>
        /// <param name="nextAfter"> The node that is the new candidate insertion point. </param>
        /// <returns> was a valid insertion point found. </returns>
        private static bool TryGetNextCandidateAfter(Node node, int linkDirection, Node head,
            Node newNode, out Node nextAfter)
        {
            nextAfter = node.Next;
            // Have we reached the limit by walking?
            if (nextAfter == head) return false;
            // If there isn't a link on the specified element then we are good to go.
            if (nextAfter.Links[linkDirection] == null || nextAfter.Links[linkDirection] == newNode) return true;
            // If there is a link and it goes to earlier than the current element then this
            // fold must be outside of the fold we are adding and therefore there are
            // no further options.
            if (nextAfter.Links[linkDirection].Index < nextAfter.Index) return false;
            // We were considering placing inside a fold so go to the other side of the
            // folded layers.
            nextAfter = nextAfter.Links[linkDirection];
            // Check if this new node is the limit and as such we should stop.
            return nextAfter != head;
        }


        /// <summary>
        /// Convert the linked list representation used while calculating to a
        /// <see cref="FoldedStack"/> instance.
        /// </summary>
        /// <param name="width"> the width of the original sheet. </param>
        /// <param name="height"> the height of the original sheet. </param>
        /// <param name="head"> the head node for the linked list. </param>
        /// <returns> a new <see cref="FoldedStack"/>. </returns>
        private static FoldedStack ToFoldedStack(in int width, in int height, Node head)
        {
            var nodes = new List<Element>();
            for (var node = head.Next; node != head; node = node.Next)
                nodes.Add(node.Element);

            return new FoldedStack(width, height, nodes);
        }


        /// <summary>
        /// Add a node to the list after another node.
        /// </summary>
        /// <param name="node"> the node to add to the list. </param>
        /// <param name="after"> the node to add <paramref name="node"/> after. </param>
        /// <param name="head"> the head of the list. </param>
        private static void AddAfter(Node node, Node after, Node head)
        {
            node.Next = after.Next;
            node.Next.Previous = node;

            after.Next = node;
            node.Previous = after;

            node.Index = after.Index + 1;
            for (var toUpdate = node.Next; toUpdate != head; toUpdate = toUpdate.Next)
                toUpdate.Index++;
        }


        /// <summary>
        /// Move a node to after a node which later than it's current position.
        /// </summary>
        /// <param name="node"> the node to move. </param>
        /// <param name="after"> the node to move <paramref name="node"/> to after. </param>
        private static void MoveToAfterLater(Node node, Node after)
        {
            Debug.Assert(node != after); // Can't go after itself;
            Debug.Assert(node.Previous != after); // Must actually be a move.
            Debug.Assert(node.Index < after.Index); // Must be moving to later.

            var oldBefore = node.Next;

            oldBefore.Previous = node.Previous;
            node.Previous.Next = oldBefore;

            node.Next = after.Next;
            node.Next.Previous = node;

            node.Previous = after;
            after.Next = node;

            node.Index = after.Index;
            for (var toUpdate = oldBefore; toUpdate != node; toUpdate = toUpdate.Next)
                toUpdate.Index--;
        }


        /// <summary>
        /// Add a node to the list before another node.
        /// </summary>
        /// <param name="node"> the node to add to the list. </param>
        /// <param name="before"> the node to add <paramref name="node"/> before. </param>
        /// <param name="head"> the head of the list. </param>
        private static void AddBefore(Node node, Node before, Node head)
        {
            node.Previous = before.Previous;
            node.Previous.Next = node;

            before.Previous = node;
            node.Next = before;

            node.Index = before.Index;
            for (var toUpdate = before; toUpdate != head; toUpdate = toUpdate.Next)
                toUpdate.Index++;
        }


        /// <summary>
        /// Move a node to before a node which erlier than it's current position.
        /// </summary>
        /// <param name="node"> the node to move. </param>
        /// <param name="before"> the node to move <paramref name="node"/> to before. </param>
        private static void MoveToBeforeEarlier(Node node, Node before)
        {
            Debug.Assert(node != before); // Can't go before itself;
            Debug.Assert(node.Next != before); // Must actually be a move.
            Debug.Assert(node.Index > before.Index); // Must be moving to later.

            var oldBefore = node.Previous;

            oldBefore.Next = node.Next;
            node.Next.Previous = oldBefore;

            node.Previous = before.Previous;
            node.Previous.Next = node;

            node.Next = before;
            before.Previous = node;

            node.Index = before.Index;
            for (var toUpdate = oldBefore; toUpdate != node; toUpdate = toUpdate.Previous)
                toUpdate.Index++;
        }


        /// <summary>
        /// Remove a node from the linked list. The state of <paramref name="node"/> is
        /// left invalid but can be made valid by adding back into the list.
        /// </summary>
        /// <param name="node"> the node to remove. </param>
        /// <param name="head"> the head of the list. </param>
        private static void RemoveNode(Node node, Node head)
        {
            node.Next.Previous = node.Previous;
            node.Previous.Next = node.Next;
            for (var aNode = node.Next; aNode != head; aNode = aNode.Next)
                aNode.Index--;
            node.Index = int.MinValue;
        }


        /// <summary>
        /// An entry in the stack of elements. This class forms a node in a doubly linked
        /// list with up to 4 extra links to represent the connections across folds in
        /// the sheet.
        /// </summary>
        [DebuggerDisplay("{Element}-{Index}")]
        [DebuggerTypeProxy(typeof(DebuggerWrapper))]
        private class Node
        {
            /// <summary>
            /// Create a new node for an element.
            /// </summary>
            /// <param name="element"></param>
            public Node(Element element)
            {
                Element = element;
                Index = int.MinValue;
            }


            public override string ToString() => $"{Element}-{Index}";

            /// <summary>
            /// The element this node is for.
            /// </summary>
            public Element Element { get; }

            /// <summary>
            /// The index within the list for this element. For any node not in the list
            /// and for the head node of a list this will be <see cref="int.MinValue"/>.
            /// </summary>
            public int Index { get; set; }

            /// <summary>
            /// The next node in the list.
            /// </summary>
            public Node Next { get; set; }

            /// <summary>
            /// The previous node in the list.
            /// </summary>
            public Node Previous { get; set; }

            /// <summary>
            /// The up to 4 extra nodes linked by being connected by a fold. The index in
            /// this list gives which side of the stack the fold is on using the
            /// constants <see cref="Left"/>, <see cref="Right"/>, <see cref="Top"/> and
            /// <see cref="Bottom"/>.
            /// </summary>
            public Node[] Links { get; } = new Node[4];


            /// <summary>
            /// A wrapper class used to make <see cref="Node"/> instances show better in
            /// the debugger mainly by showing the links as separate properties rather
            /// than an array.
            /// </summary>
            private class DebuggerWrapper
            {
                public DebuggerWrapper(Node node)
                {
                    _node = node;
                }


                public Element Element => _node.Element;
                public int Index => _node.Index;
                public Node Previous => _node.Previous;
                public Node Next => _node.Next;
                public Node LinkLeft => _node.Links[Left];
                public Node LinkRight => _node.Links[Right];
                public Node LinkTop => _node.Links[Top];
                public Node LinkBottom => _node.Links[Bottom];

                private readonly Node _node;
            }
        }


        private const int Left = 0;
        private const int Right = 1;
        private const int Top = 2;
        private const int Bottom = 3;
    }
}