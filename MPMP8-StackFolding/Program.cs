﻿using System;

namespace MPMP8_StackFolding
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            int width;
            int height;
            string method;
            if (args.Length == 3)
            {
                width = int.Parse(args[0]);
                height = int.Parse(args[1]);
                method = args[2];
            }
            else
            {
                Console.Write("Enter the width: ");
                width = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                Console.Write("Enter the height: ");
                height = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                Console.WriteLine("What would you like to do? Options are 'all', 'valid', 'all zero' or 'valid zero'");
                Console.Write(": ");
                method = Console.ReadLine();
            }

            long i = 0;
            var options = method switch
            {
                "all" => FoldedStack.GenerateAll(width, height),
                "valid" => StackBuilder.GenerateValidStacks(width, height, false),
                "all zero" => FoldedStack.GenerateAllWithZeroFixed(width, height),
                "valid zero" => StackBuilder.GenerateValidStacks(width, height, true),
                _ => throw new Exception("Invalid request")
            };

            foreach (var stack in options)
                Console.WriteLine($"Option {++i}\n{stack:H}");

            Console.WriteLine($"Produced {i} Options");
        }
    }
}